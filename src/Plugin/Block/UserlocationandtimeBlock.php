<?php

namespace Drupal\userlocationandtime\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\userlocationandtime\Service\TimeFormatter;

/**
 * Provides a 'User Location and Time' Block.
 *
 * @Block(
 *   id = "User Location and Time",
 *   admin_label = @Translation("User Location and Time Details"),
 *   category = @Translation("User Location and Time Details"),
 * )
 */
class UserlocationandtimeBlock extends BlockBase implements
  ContainerFactoryPluginInterface {

  /**
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  private $config;

  /**
   * @var Drupal\userlocationandtime\Service
   */
  private $timeformatter;

  /**
   * Constructs a User Location and Time object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config instance.
   * @param Drupal\userlocationandtime\Service\TimeFormatter $timeformatter
   *   TimeFormatter instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $configFactory, TimeFormatter $timeformatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $configFactory;
    $this->timeformatter = $timeformatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('userlocationandtime.timeformatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get data from config.
    $config = $this->config->get('userlocation.settings');
    $country = $config->get('country');
    $city = $config->get('city');
    // Called time formatter service.
    $timezone = $this->timeformatter->getFormattedTime();

    return [
      '#theme' => 'user_location_details',
      '#user_country' => $country,
      '#user_city' => $city,
      '#user_timezone' => $timezone,
      '#cache' => [
        'tags' => ['config:userlocation.settings'], //Invalidate when user location config updates
      ]
    ];
  }

}