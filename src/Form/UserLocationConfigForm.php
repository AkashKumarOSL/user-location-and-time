<?php

namespace Drupal\userlocationandtime\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UserLocationConfigForm.
 */
class UserLocationConfigForm extends ConfigFormBase {

  protected $timezone_options;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'userlocation.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_location_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('userlocation.settings');

    // Timezone select options.
    $this->timezone_options = [
      'America/Chicago' => t('America/Chicago'),
      'America/New_York' => t('America/New_York'),
      'Asia/Tokyo' => t('Asia/Tokyo'),
      'Asia/Dubai' => t('Asia/Dubai'),
      'Asia/Kolkata' => t('Asia/Kolkata'),
      'Europe/Amsterdam' => t('Europe/Amsterdam'),
      'Europe/Oslo' => t('Europe/Oslo'),
      'Europe/London' => t('Europe/London'),
    ];

    $form['user_setting'] = [
      '#type' => 'details',
      '#title' => t('User Location Details'),
      '#collapsible' => TRUE,
      '#open' => TRUE,
      '#description' => t('Add location and timezone details.'),
    ];

    $form['user_setting']['country'] = [
      '#type' => 'textfield',
      '#title' => t('Country'),
      '#default_value' => $config->get('country'),
      '#required' => TRUE,
      '#size' => 100,
      '#maxlength' => 150,
      '#description' => t('Add country name.'),
    ];

    $form['user_setting']['city'] = [
      '#type' => 'textfield',
      '#title' => t('City'),
      '#default_value' => $config->get('city'),
      '#required' => TRUE,
      '#description' => t('Add city name.'),
    ];

    $form['user_setting']['timezone'] = [
      '#type' => 'select',
      '#title' => t('Timezone'),
      '#options' => $this->timezone_options,
      '#required' => TRUE,
      '#default_value' => $config->get('timezone'),
      '#description' => t('Select timezone.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('userlocation.settings')
      ->set('country', $form_state->getValue('country'))
      ->set('city', $form_state->getValue('city'))
      ->set('timezone', $form_state->getValue('timezone'))
      ->save();
    $this->messenger()->addMessage(t('Country: %country, City: %city and Timezone: %timezone has been saved succesfully.', ['%country' => $form_state->getValue('country'), '%city' => $form_state->getValue('city'), '%timezone' => $this->timezone_options[$form_state->getValue('timezone')]]));

    parent::submitForm($form, $form_state);
  }

}
