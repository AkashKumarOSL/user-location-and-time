<?php

namespace Drupal\userlocationandtime\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Class TimeFormatter used to format time as per the timezone.
 *
 * @package \Drupal\userlocationandtime\Service
 */
class TimeFormatter {

  /**
   * @var  Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * TimeFormatter service constructor.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->config = $configFactory;
  }

  /**
   * Returns string of formatted time.
   */
  public function getFormattedTime() {
    // Get selected timezone form config.
    $config = $this->config->get('userlocation.settings');
    $timezone = $config->get('timezone');
    // Convert current time to as per selected timezone.
    $date = new \DateTime('now', new \DateTimeZone('UTC'));
    $date->setTimezone(new \DateTimeZone($timezone));
    $current_time = $date->format('dS M Y - h:i:s A');

    return $current_time;
  }

}