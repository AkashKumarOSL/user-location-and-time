CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Installation
  * Configuration
  * Maintainers


INTRODUCTION
------------

Module provides a config form to set user location and timezone. It supports a custom block(with the config form values) to render user location and time. For the formatted timezone, we are using our custom service.  


INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module.
    See: https://www.drupal.org/node/895232 for further information.
  * Place the custom block name(User Location and Time) from the block
    layout page(/admin/structure/block).


CONFIGURATION
-------------

  * Configure the user location details at (/admin/config/userlocation).


MAINTAINERS
-----------

Current maintainers:
  Akash Kumar(https://www.drupal.org/u/akashkumarosl)